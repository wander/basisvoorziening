using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Wander
{
    [ExecuteAlways()]
    public class BasisVoorziening : MonoBehaviour
    {
        public string fileName;
        public string rootFolder;
        public float offsetX = 191340;
        public float offsetY = 443070;
        public bool swapIndices02;
        public Material notFoundMat;
        public bool mergeResult = true;
        public bool saveAssetsToDisk = true;
        public bool flattenTerrainHeight = false;
        public bool flattenWaterObject = false;
        public bool decimateGeometry = true;

        public Material matBridge;
        public Material matBuilding;
        public Material matCityFurniture;
        public Material matLandUse;
        public Material matOtherConstruction;
        public Material matPlantCover;
        public Material matSolitaryVegationObject;
        public Material matTINRelief;
        public Material matTransportation;
        public Material matRoad;
        public Material matRailway;
        public Material matWaterway;
        public Material matTunnel;
        public Material matWaterBody;
        
        public List<string> skipObjects   = new List<string>{ "Building", "BuildingInstallation" };
        public List<string> smoothNormals = new List<string>{ "TINRelief", "WaterBody"};

        internal HeightData heightData;
        public HeightData HeightData => heightData;

        internal class CityJsonPostProcessResult
        {
            internal string filename;
            internal List<(MeshCombine.Input, string, List<CityMaterial>)> inputs;
        }

        List<Task<CityJsonPostProcessResult>> generateTask = new List<Task<CityJsonPostProcessResult>>();
        Dictionary<string, Material> typeToMat;

        private void Update()
        {
            if (!Application.isEditor)
            {
                enabled = false;
                return;
            }

            for (int i = 0; i < generateTask.Count; i++)
            {
                if ( generateTask[i].IsCompleted )
                {
                    if (generateTask[i].IsCompletedSuccessfully &&
                        generateTask[i].Result != null)
                    {
                        print( "Start Building Unity Objects " + i );
                        BuildUnityGameObjects( generateTask[i].Result );
                        print( "Finished Building Unity Objects " + i );
                    }
                    generateTask.RemoveAt(i);
                    break;  // handle others in new frame
                }
            }
        }

        internal HeightData LoadHeightData( string filename )
        {
            var fileParts = Path.GetFileNameWithoutExtension(fileName).Split( ',', '_', '.' );
            List<string> paths = new List<string>();
            paths.AddRange( Directory.GetFiles( rootFolder, "*.tiff", SearchOption.AllDirectories ) );
            paths.AddRange( Directory.GetFiles( rootFolder, "*.tif", SearchOption.AllDirectories ) );
            for( int i = 0; i < paths.Count; i++ )
            {
                var tiffParts =  Path.GetFileNameWithoutExtension(paths[i]).Split( ',', '_', '.' );
                for (int j = 0;j < tiffParts.Length;j++)
                {
                    for (int k = 0;k < fileParts.Length;k++)
                    {
                        if (tiffParts[j].ToLower() == fileParts[k].ToLower() )
                        {
                            // found matching height data
                            return GeoTiffHeight.Load( paths[i], true );
                        }
                    }
                }
            }
            return default;
        }

        internal void Generate( string _filename, HeightData heightData )
        {
            var task = Task.Run( () =>
            {
                try
                {
                    Debug.Log( "Started reading json content as text from disk" );
                    string json = File.ReadAllText( _filename );
                    Debug.Log( "Finished reading json content as text from disk" );

                    // This call will prepare data for direct use in engine. It provides list of geometries (triangle lists and indices).
                    Debug.Log( "Start parsing: " + _filename );
                    var cj = new CityJson( json, swapIndices02 );
                    Debug.Log( "Finished parsing: " + _filename );
                    json=null;

                    // Print profiling info & errors
                    foreach (var kvp in cj.Timings) print( kvp.Key + " took " + kvp.Value +"ms" );
                    foreach (var kvp in cj.Errors) UnityEngine.Debug.LogWarning( kvp.Key + " occurred " + kvp.Value + " times" );

                    Debug.Log( "Start preparing CityJson: " + _filename );
                    var inputs = PrepareCityJsonForUnity(cj, _filename, heightData );
                    Debug.Log( "Finished preparing CityJson: " + _filename );

                    cj=null;
                    CityJsonPostProcessResult res = new CityJsonPostProcessResult();
                    res.inputs = inputs;
                    res.filename = _filename;
                    return res;
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogException( e );
                }
                return null;
            } );
            generateTask.Add( task );
        }

        List<(MeshCombine.Input mcb, string type, List<CityMaterial> mats)> PrepareCityJsonForUnity(CityJson cj, string filename, HeightData heightData )
        {
            if (cj == null)
                return default;

            var unityVertices = ConvertToUnityVertices( cj, heightData );
            var unityUvs      = GenerateAllUvs( unityVertices );

            List<(MeshCombine.Input, string type, List<CityMaterial>)> combinedInputs = new List<(MeshCombine.Input, string type, List<CityMaterial>)>();

            for (int i = 0;i < cj.CityObjects.Count;i++)
            {
                var type = cj.CityObjects[i].type;
                var geom = cj.CityObjects[i].geometry;
                Debug.Assert( geom != null );
                Debug.Assert( type != null );

                if (skipObjects.Contains( type ))
                    continue;

                if (decimateGeometry)
                {
                    DecimateGeometry( cj.Vertices, geom ); 
                }

                // Make hard or smooth normals
                (List<Vector3> vertices, List<Vector2> uvs, List<int> indices) buildResult;
                if (smoothNormals.Contains( type ))
                    buildResult = BuildVerticesAndIndicesSmooth( geom.indices, unityVertices, unityUvs );
                else
                    buildResult = BuildVerticesAndIndicesHard( geom.indices, unityVertices, unityUvs ); 

                // Flatten water surface (for some reason it contains different height values).
                if (flattenWaterObject && type == "WaterBody")
                {
                    float waterLevel = 200;
                    buildResult.vertices.ForEach( v => 
                    {
                        if (v.y > -6) // minimum NAP
                        {
                            waterLevel = Mathf.Min( v.y, waterLevel );
                        }
                    } );
                    for (int j = 0;j < buildResult.vertices.Count;j++)
                    {
                        buildResult.vertices[j] = new Vector3( buildResult.vertices[j].x, waterLevel, buildResult.vertices[j].z );
                    }
                }

                MeshCombine.Input ip = new MeshCombine.Input();
                ip.indices      = new List<int>[1];
                ip.indices[0]   = buildResult.indices;
                ip.vertices     = buildResult.vertices;
                ip.uvs          = buildResult.uvs;
                ip.localToWorld = Matrix4x4.identity;
                combinedInputs.Add( (ip, type, geom.materials) );
            }

            return combinedInputs;
        }

        void BuildUnityGameObjects( CityJsonPostProcessResult result )
        {
            SetUpTypeToMaterial();
            var inputs = result.inputs;
            for(int i=0; i<inputs.Count;i++)
            {
                var inp = inputs[i];
                var cmb  = inp.Item1;
                var type = inp.Item2;
                var mats = inp.Item3;
                var mat  = FindMaterial( type, mats );
                cmb.materials = new Material[] { mat };
            }

            var blockName = Path.GetFileNameWithoutExtension( result.filename );
            GameObject cityJson = new GameObject( blockName );

            if (mergeResult)
            {
                MeshCombine mc = new MeshCombine();
                Dictionary<int, MeshCombine.Combined> combinedMeshes = mc.Combine( inputs.Select(inp=>inp.Item1).ToList() );
                foreach (var kvp in combinedMeshes)
                {
                    var cb = kvp.Value;

                    // Mesh
                    Mesh m = new Mesh();
                    m.indexFormat  = UnityEngine.Rendering.IndexFormat.UInt32;
                    m.subMeshCount = 1;
                    m.SetVertices( cb.vertices );
                    m.SetUVs( 0, cb.uv );
                    m.SetTriangles( cb.indices, 0 );
                    m.RecalculateNormals();
                    m.Optimize();
                    m.UploadMeshData( true );

#if UNITY_EDITOR
                    if (saveAssetsToDisk)
                    {
                        m = SaveMeshToDisk( m, blockName + "_" + cb.mat.name + ".mesh" );
                    }
#endif

                    // GameObject
                    GameObject go = new GameObject( cb.mat.name );
                    go.AddComponent<MeshRenderer>().sharedMaterial = cb.mat;
                    go.AddComponent<MeshFilter>().sharedMesh = m;

                    // Link
                    go.transform.SetParent( cityJson.transform );
                }
            }
            else
            {
                for (int i = 0;i<inputs.Count;i++)
                {
                    var inp = inputs[i];
                    var cmb  = inp.Item1;
                    var type = inp.Item2;
                    var mats = inp.Item3;

                    Debug.Assert( cmb.materials.Length == 1 );
                    Debug.Assert( cmb.indices.Length == 1 );

                    Mesh m = new Mesh();
                    m.indexFormat  = UnityEngine.Rendering.IndexFormat.UInt32;
                    m.subMeshCount = 1;
                    m.SetVertices( cmb.vertices );
                    m.SetUVs( 0, cmb.uvs );
                    m.SetTriangles( cmb.indices[0], 0 );
                    m.RecalculateNormals();
                    m.Optimize();
                    m.UploadMeshData( true );

                    // GameObject
                    GameObject go = new GameObject( cmb.materials[0].name );
                    go.AddComponent<MeshRenderer>().sharedMaterial = cmb.materials[0];
                    go.AddComponent<MeshFilter>().sharedMesh = m;

                    // Link
                    go.transform.SetParent( cityJson.transform );
                }
            }
        }

#if UNITY_EDITOR
        Mesh SaveMeshToDisk(Mesh m, string name)
        {
            var generatedContent = "BasisVoorziening_" + offsetX + "_" + offsetY;
            if (!Directory.Exists( Path.Combine( Application.dataPath, generatedContent ) ))
            {
                Directory.CreateDirectory( Path.Combine( Application.dataPath, generatedContent ) );
            }
            AssetDatabase.CreateAsset( m, Path.Combine( "Assets", generatedContent, name ));
            return m;
        }
#endif

        void SetUpTypeToMaterial()
        {
            typeToMat = new Dictionary<string, Material>();
            typeToMat.Add( "Bridge", matBridge );
            typeToMat.Add( "CityFurniture", matCityFurniture );
            typeToMat.Add( "LandUse", matLandUse );
            typeToMat.Add( "OtherConstruction", matOtherConstruction );
            typeToMat.Add( "PlantCover", matPlantCover );
            typeToMat.Add( "SolitaryVegationObject", matSolitaryVegationObject );
            typeToMat.Add( "TINRelief", matTINRelief );
            typeToMat.Add( "Transportation", matTransportation );
            typeToMat.Add( "Road", matRoad );
            typeToMat.Add( "Railway", matRailway );
            typeToMat.Add( "Waterway", matWaterway );
            typeToMat.Add( "Tunnel", matTunnel );
            typeToMat.Add( "WaterBody", matWaterBody );
        }

        Material FindMaterial( string type, List<CityMaterial> materials )
        {
            if (materials != null && materials.Count > 0)
            {
                var diffColor = materials[0].diffuseColor;
                Material mat = new Material( notFoundMat );
                mat.color = new Color( diffColor.X, diffColor.Y, diffColor.Z );
                return mat;
            }
            else
            {
                if ( typeToMat.TryGetValue( type, out Material mat ) )
                {
                    return mat;
                }
            }
            return notFoundMat;
        }

        class CompareEdges : IComparer<(int e1, int e2)>, IComparer
        {
            public int Compare( (int e1, int e2) x, (int e1, int e2) y)
            {
                if (x.e1 < y.e1)
                    return -1;
                if ( x.e1 == y.e1 )
                {
                    if (x.e2 < y.e2)
                        return -1;
                    if (x.e2 > y.e2)
                        return 1;
                    return 0;
                }
                return 1;

                //int d1 = x.e1 - y.e1;
                //int d2 = x.e2 - y.e2;
                
                //if (d1 < 0 || (d1==0 && d2<0) )
                //    return -1;

                //if (d1 > 0 || (d1==0 && d2>0) )
                //    return 1;

                //Debug.Assert( (d1==d2) && d1==0 );
                //return 0;
            }

            public int Compare( object x, object y )
            {
                var edge1 = ((int e1, int e2))x;
                var edge2 = ((int e1, int e2))y;
                return Compare( edge1, edge2 );
            }
        }

        static void UpdateEdgeList( SortedDictionary<(int e1, int e2), (int t1, int t2)> edgeList, int v0, int v1, int t )
        {
            if (!edgeList.TryGetValue( (v0, v1), out (int t1, int t2) value ))
            {
                if (!edgeList.TryGetValue( (v1, v0), out (int t1, int t2) value2 ))
                {
                    edgeList.Add( (v1, v0), (t, -1) );
                }
                else
                {
                    Debug.Assert( value2.t2==-1 );
                    edgeList.Remove( (v1, v0) );
                    edgeList.Add( (v1, v0), (value2.t1, t) );
                }
            }
            else
            {
                if ( value.t2 != -1 )
                {
                    int j = 0;
                }
     //           Debug.Assert( value.t2==-1 );
                edgeList.Remove( (v0, v1) );
                edgeList.Add( (v0, v1), (value.t1, t) );
            }
        }

        Vector3 GetNormal( List<(long x, long y, long z)> vertices, int vIdx0, int vIdx1, int vIdx2 )
        {
            Vector3 v0 = new Vector3(vertices[vIdx0].x, vertices[vIdx0].y, vertices[vIdx0].z);
            Vector3 v1 = new Vector3(vertices[vIdx1].x, vertices[vIdx1].y, vertices[vIdx1].z);
            Vector3 v2 = new Vector3(vertices[vIdx2].x, vertices[vIdx2].y, vertices[vIdx2].z);
            return Vector3.Normalize( Vector3.Cross( v1-v0, v2-v0 ) );
        }

        void DecimateGeometry( List<(long x, long y, long z)> vertices, CityGeometry geom )
        {
            /*  The idea of this function is to only keep the outerior ring of the surface and re-topologize it to reduce vertex/triangle count.
             * 
             * */

            if (geom.hasInteriorRings)
                return; // Algorithm doesnt work on this.

            var indices = geom.indices;

            // Get normal of surface, to ensure winding order is correct after Tesselate.
            var v0 = indices[0];
            var v1 = indices[1];
            var v2 = indices[2];
            var surfNormal = GetNormal( vertices, v0, v1, v2 );

            // Determine projection side
            int projectSide = 2;
            float absX = MathF.Abs(surfNormal.x);
            float absY = MathF.Abs(surfNormal.y);
            float absZ = MathF.Abs(surfNormal.z);
            if ((absX > absY) && (absX > absZ))
            {
                projectSide = 0;
            }
            else if (absY > absZ)
            {
                projectSide = 1;
            }

            // For each triangle, pick the 3 edges and put the edges as key in the dictionary.
            // Each edge is shared by two triangles except on the outerior ring. 
            // This is exactly what we want. Keep the outerior ring and use the vertices from that
            // to create a new triangulated polygon without all the intermediate vertices.
            var edgeList = new SortedDictionary<(int e1, int e2), (int t1, int t2)>(new CompareEdges());

            for ( int i=0; i<indices.Count; i+=3 )
            {
                v0 = indices[i+0];
                v1 = indices[i+1];
                v2 = indices[i+2];
                UpdateEdgeList( edgeList, v0, v1, i );
                UpdateEdgeList( edgeList, v1, v2, i );
                UpdateEdgeList( edgeList, v2, v0, i );
            }

            List<int> uniqueVertices = new List<int>();
            if ( edgeList.Count >= 3 )
            {
                foreach (var edgeTriPair in edgeList)
                {
                    var edge    = edgeTriPair.Key;
                    var triPair = edgeTriPair.Value;
                    if (triPair.t2==-1) // This edge has no two triangles, so is on outerior ring.
                    {
                        uniqueVertices.Add( edge.e1 );
                        uniqueVertices.Add( edge.e2 );
                        break;
                    }
                }

                int vidx = 1;
                while (true)
                {
                    int e1 = uniqueVertices[vidx-1];
                    int e2 = uniqueVertices[vidx+0];
                    foreach (var edgeTriPair in edgeList)
                    {
                        var edge = edgeTriPair.Key;
                        if ( (edge.e1 == e2 && edge.e2 != e1))
                        {
                            uniqueVertices.Add( edge.e1 );
                            vidx++;
                            break;
                        }
                        else if (edge.e2 == e2 && edge.e1 != e1)
                        {
                            uniqueVertices.Add( edge.e2 );
                            vidx++;
                            break;
                        }
                    }

                    if (uniqueVertices[uniqueVertices.Count-1] == uniqueVertices[0])
                        break;
                }
            }

            List<double> projectedVertices = new List<double>();
            List<int> originalIndices = new List<int>();
            for (int i = 0; i < uniqueVertices.Count; i++)
            {
                int vertexIndex = uniqueVertices[i];
                originalIndices.Add( vertexIndex );
                if (projectSide == 0)
                {
                    projectedVertices.Add( vertices[vertexIndex].y );
                    projectedVertices.Add( vertices[vertexIndex].z );
                }
                else if (projectSide == 1)
                {
                    projectedVertices.Add( vertices[vertexIndex].x );
                    projectedVertices.Add( vertices[vertexIndex].z );
                }
                else
                {
                    projectedVertices.Add( vertices[vertexIndex].x );
                    projectedVertices.Add( vertices[vertexIndex].y );
                }
            }

            var triangulatedIndices = EarcutNet.Earcut.Tessellate( projectedVertices, new List<int>() );

            if (triangulatedIndices.Count >= 3)
            {
                // Check triangle Normal against Surface normal, if dot is negative, flip indices to preserve winding order.
                var triNormal = GetNormal( vertices,
                        originalIndices[triangulatedIndices[0]],
                        originalIndices[triangulatedIndices[1]],
                        originalIndices[triangulatedIndices[2]]
                        );

                bool mustSwapIndices = (Vector3.Dot( triNormal, surfNormal ) < 0);
                geom.indices.Clear();
                for (int i = 0;i < triangulatedIndices.Count;i += 3)
                {
                    if (!mustSwapIndices)
                    {
                        geom.indices.Add( originalIndices[triangulatedIndices[i+0]] );
                        geom.indices.Add( originalIndices[triangulatedIndices[i+1]] );
                        geom.indices.Add( originalIndices[triangulatedIndices[i+2]] );
                    }
                    else
                    {
                        geom.indices.Add( originalIndices[triangulatedIndices[i+2]] );
                        geom.indices.Add( originalIndices[triangulatedIndices[i+1]] );
                        geom.indices.Add( originalIndices[triangulatedIndices[i+0]] );
                    }
                }
            }
        }

        (List<Vector3> vertices, List<Vector2>, List<int> indices) BuildVerticesAndIndicesSmooth(List<int> indicesIn, List<Vector3> verticesIn, List<Vector2> uvsIn)
        {
            var indices  = new List<int>();
            var indicesMap = new Dictionary<int, (Vector3 p, Vector2 uv, int idx)>();

            for (int j = 0;j < indicesIn.Count;j++)
            {
                int olddIdx = indicesIn[j];
                int newIdx  = indicesMap.Count;
                if (!indicesMap.TryGetValue( olddIdx, out (Vector3 p, Vector2 uv, int idx) v ))
                {
                    indicesMap.Add( olddIdx, (verticesIn[olddIdx], uvsIn[olddIdx], newIdx) );
                }
                else
                {
                    newIdx = v.idx;
                }
                indices.Add( newIdx );
            }

            var vertices = new List<Vector3>(indicesMap.Count);
            var uvs      = new List<Vector2>(indicesMap.Count);
            int lastAddedIdx = -1;
            foreach (var kvp in indicesMap)
            {
                int addIdx = kvp.Value.idx;
                int kAdds  = (kvp.Value.idx - lastAddedIdx);
                while (kAdds-- > 0)
                {
                    vertices.Add( Vector3.zero );
                    uvs.Add( Vector2.zero );
                }
                vertices[addIdx] = kvp.Value.p;
                uvs[addIdx]      = kvp.Value.uv;
                lastAddedIdx     = addIdx;
            }

            return (vertices, uvs, indices);
        }

        (List<Vector3> vertices, List<Vector2> uvs, List<int> indices) BuildVerticesAndIndicesHard( List<int> indicesIn, List<Vector3> verticesIn, List<Vector2> uvsIn )
        {
            var vertices = new List<Vector3>(indicesIn.Count);
            var uvs      = new List<Vector2>(indicesIn.Count);
            var indices  = new List<int>(indicesIn.Count);
            for (int j = 0;j < indicesIn.Count;j += 3)
            {
                int v0 = indicesIn[j];
                int v1 = indicesIn[j+1];
                int v2 = indicesIn[j+2];
                vertices.Add( verticesIn[v0] );
                vertices.Add( verticesIn[v1] );
                vertices.Add( verticesIn[v2] );
                uvs.Add( uvsIn[v0] );
                uvs.Add( uvsIn[v1] );
                uvs.Add( uvsIn[v2] );
                indices.Add( j );
                indices.Add( j+1 );
                indices.Add( j+2 );
            }
            return (vertices, uvs, indices);
        }

        List<Vector3> ConvertToUnityVertices(CityJson cj, HeightData heightData)
        {
            var sc  = cj.Scale;
            var tsl = cj.Translate;
            Vector3 offset = new Vector3( tsl.X, tsl.Y, tsl.Z );
            var unityVertices = cj.Vertices.Select( v =>
            {
                double x = ((v.x * (double)sc.X + offset.x));
                double z = ((v.y * (double)sc.Y + offset.y));
                double y = ((v.z * (double)sc.Z + offset.z));
                if ( heightData.data != null )
                {
                    y = heightData.FromRD( x, z, out bool wasFound, true );
                }
                if ( flattenTerrainHeight )
                {
                    y = 0;
                }
                x -= offsetX;
                z -= offsetY;
                var vNew = new Vector3((float)x, (float)y, (float)z);
                return vNew;
            } ).ToList();
            return unityVertices;
        }

        List<Vector2> GenerateAllUvs( List<Vector3> unityVertices )
        {
            List<Vector2> uvs = new List<Vector2>(unityVertices.Count);
            for (int i = 0;i<unityVertices.Count;i++)
            {
                uvs.Add(new Vector2( unityVertices[i].x, unityVertices[i].z));
            }
            return uvs;
        }
    }



#if UNITY_EDITOR

    [CustomEditor( typeof( BasisVoorziening ) )]
    [InitializeOnLoad]
    public class BasisVoorzieningEditor : Editor
    {
        static BasisVoorzieningEditor()
        {

        }

        public override void OnInspectorGUI()
        {
            BasisVoorziening bsv = (BasisVoorziening)target;

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Select file" ))
                {
                    string s = EditorUtility.OpenFilePanel( bsv.fileName, "", "json" );
                    if (!string.IsNullOrWhiteSpace( s ))
                    {
                        bsv.fileName = s;
                    }
                }

                if (GUILayout.Button( "Go from file" ))
                {
                    bsv.rootFolder = Path.GetDirectoryName( bsv.fileName );
                    bsv.heightData = bsv.LoadHeightData( bsv.fileName );
                    bsv.Generate( bsv.fileName, bsv.heightData );
                }

                GUILayout.Space(10);

                if (GUILayout.Button( "Select folder" ))
                {
                    string s = EditorUtility.OpenFolderPanel( "Select folder", bsv.rootFolder, "" );
                    if (!string.IsNullOrWhiteSpace( s ))
                    {
                        bsv.rootFolder = s;
                    }
                }

                if ( GUILayout.Button( "Go from folder" ) )
                {
                    string [] files = Directory.GetFiles( bsv.rootFolder, "*.json" );
                    if (files.Length > 0)
                    {
                        bsv.heightData = bsv.LoadHeightData( files[0] );
                        for (int i = 0;i < files.Length;i++)
                        {
                            bsv.Generate( files[i], bsv.heightData );
                        }
                    }
                }
            }
            GUILayout.EndHorizontal();


            DrawDefaultInspector();
        }
    }

#endif
}
